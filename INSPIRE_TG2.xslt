<?xml version="1.0" encoding="UTF-8"?>
<!-- Licensed under the EUPL, Version 1.2 (the "Licence");
You may not use this work except in compliance with the Licence.
You may obtain a copy of the Licence at:
https://joinup.ec.europa.eu/sites/default/files/custom-page/attachment/eupl_v1.2_sk.pdf
Date: 24/12/2020
MZP SR - Ministerstvo životného prostredia Slovenskej republiky (Ministry of Environment of the Slovak Republic) 
- Created by SEVITECH under the contract of Spatial data registry Service layer agreement  - 
inspire@enviro.gov.sk  -->
<xsl:stylesheet 
    xmlns:gml="http://www.opengis.net/gml"   
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:gco="http://www.isotc211.org/2005/gco" 
    xmlns:gfc="http://www.isotc211.org/2005/gfc" 
    xmlns:gmd="http://www.isotc211.org/2005/gmd" 
    xmlns:gmx="http://www.isotc211.org/2005/gmx" 
    xmlns:gsr="http://www.isotc211.org/2005/gsr" 
    xmlns:gss="http://www.isotc211.org/2005/gss" 
    xmlns:gts="http://www.isotc211.org/2005/gts" 
    xmlns:srv="http://www.isotc211.org/2005/srv" 
    xmlns:xlink="http://www.w3.org/1999/xlink" 
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
    version="1.0" 
    xsi:schemaLocation="http://www.isotc211.org/2005/gmd https://inspire.ec.europa.eu/draft-schemas/inspire-md-schemas-temp/apiso-inspire/apiso-inspire.xsd">
    <xsl:output method="xml" encoding="UTF-8" indent="yes"/>
    <!-- globálne	premenné 	-->
    <xsl:variable name="lowercase" select="'aábcdefghiíjklmnňopqrstuúvwxyžz/:.-'"/>
    <xsl:variable name="uppercase" select="'AÁBCDEFGHIÍJKLMNŇOPQRSTUÚVWXYŽZ/:.-'"/>
    <!-- hierarchyLevel -->
    <xsl:variable name="hierarchyLevel">
        <xsl:if test="//gmd:hierarchyLevel/gmd:MD_ScopeCode/@codeListValue = 'service'">
            <xsl:value-of select="'service'"/>
        </xsl:if>
    </xsl:variable>

    <!-- serviceType -->
    <xsl:variable name="serviceType">
        <xsl:value-of select="//gmd:identificationInfo/srv:SV_ServiceIdentification/srv:serviceType/gco:LocalName/text()"/>
    </xsl:variable>

    <!-- Templates -->

    <!-- Skopirovanie celeho mtd zaznamu -->

    <xsl:template name="HeaderTemplate" match="@* | node()">
        <xsl:copy>            
            <xsl:apply-templates select="@* | node()"/>
         
        </xsl:copy>
    </xsl:template>
     
     
    <!-- úprava referencii na schemy -->
    <xsl:template match="@xsi:schemaLocation"/>
    <xsl:template name="Namespaces" match="//gmd:MD_Metadata">

        <xsl:choose>
            <xsl:when test="$hierarchyLevel = 'service'">

                <gmd:MD_Metadata xmlns:gml="http://www.opengis.net/gml/3.2" xsi:schemaLocation="http://www.isotc211.org/2005/srv http://inspire.ec.europa.eu/draft-schemas/inspire-md-schemas-temp/apiso-inspire/apiso-inspire.xsd">
                    <xsl:apply-templates select="@* | node()"/>
                </gmd:MD_Metadata>
            </xsl:when>
            <xsl:otherwise>
                <gmd:MD_Metadata xmlns:gml="http://www.opengis.net/gml/3.2" xsi:schemaLocation="http://www.isotc211.org/2005/gmd https://inspire.ec.europa.eu/draft-schemas/inspire-md-schemas-temp/apiso-inspire/apiso-inspire.xsd">
                    <xsl:apply-templates select="@* | node()"/>
                </gmd:MD_Metadata>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- Hierarchy level - level description pre service -->
    <xsl:template match="//gmd:hierarchyLevel">
        <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>
        <!-- Hierarchy level name -->
        <xsl:if test="gmd:MD_ScopeCode/@codeListValue = 'service'">
            <gmd:hierarchyLevelName>
                <gco:CharacterString>Service</gco:CharacterString> 
            </gmd:hierarchyLevelName>
        </xsl:if>
    </xsl:template>
    <xsl:template match="//gmd:hierarchyLevelName"/>

    <!-- Kľúčové slová INSPIRE GEMET  -->
    <xsl:template match="//gmd:descriptiveKeywords/gmd:MD_Keywords[gmd:thesaurusName/gmd:CI_Citation/gmd:title/gco:CharacterString[starts-with(., 'GEMET - INSPIRE themes')]]">
        <gmd:MD_Keywords>
            <xsl:for-each select="gmd:keyword">
                <gmd:keyword>
                    <xsl:if test="gco:CharacterString = 'Atmospheric conditions'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/ac">Atmosférické podmienky</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Atmosférické podmienky'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/ac">Atmosférické podmienky</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Production and industrial facilities'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/pf">Výrobné a priemyselné zariadenia</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Výrobné a priemyselné zariadenia'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/pf">Výrobné a priemyselné zariadenia</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Land cover'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/lc">Krajinná pokrývka (land cover)</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Krajinná pokrývka (land cover)'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/lc">Krajinná pokrývka (land cover)</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Bio-geographical regions'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/br">Biogeografické regióny</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Biogeografické regióny'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/br">Biogeografické regióny</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Population distribution — demography'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/br">Rozmiestnenie obyvateľstva – demografia</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Rozmiestnenie obyvateľstva – demografia'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/br">Rozmiestnenie obyvateľstva – demografia</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Sea regions'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/sr">Morské regióny</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Morské regióny'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/sr">Morské regióny</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Species distribution'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/sd">Výskyt druhov</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Výskyt druhov'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/sd">Výskyt druhov</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Transport networks'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/tn">Dopravné siete</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Dopravné siete'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/tn">Dopravné siete</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Buildings'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/bu">Stavby</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Stavby'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/bu">Stavby</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Energy resources'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/er">Zdroje energie</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Zdroje energie'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/er">Zdroje energie</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Meteorological geographical features'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/mf">Meteorologické geografické prvky</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Meteorologické geografické prvky'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/mf">Meteorologické geografické prvky</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Mineral resources'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/mr">Zdroje nerastných surovín</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Zdroje nerastných surovín'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/mr">Zdroje nerastných surovín</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Oceanographic geographical features'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/of">Oceánografické geografické prvky</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Oceánografické geografické prvky'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/of">Oceánografické geografické prvky</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Human health and safety'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/hh">Ľudské zdravie a bezpečnosť</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Ľudské zdravie a bezpečnosť'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/hh">Ľudské zdravie a bezpečnosť</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Elevation'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/el">Výška</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Výška'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/el">Výška</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Utility and governmental services'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/us">Verejné a štátne služby</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Verejné a štátne služby'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/us">Verejné a štátne služby</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Geology'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/ge">Geológia</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Geológia'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/ge">Geológia</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Coordinate reference systems'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/rs">Súradnicové referenčné systémy</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Súradnicové referenčné systémy'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/rs">Súradnicové referenčné systémy</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Habitats and biotopes'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/hb">Habitaty a biotopy</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Habitaty a biotopy'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/hb">Habitaty a biotopy</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Geographical grid systems'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/gg">Geografické systémy sietí</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Geografické systémy sietí'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/gg">Geografické systémy sietí</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Hydrography'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/hy">Hydrography</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Hydrografia'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/hy">Hydrografia</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Protected sites'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/ps">Chránené územia</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Chránené územia'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/ps">Chránené územia</gmx:Anchor>
                    </xsl:if>
                     <xsl:if test="gco:CharacterString = 'Chráněná území'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/ps">Chránené územia</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Agricultural and aquaculture facilities'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/af">Poľnohospodárske zariadenia a zariadenia akvakultúry</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Poľnohospodárske zariadenia a zariadenia akvakultúry'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/af">Poľnohospodárske zariadenia a zariadenia akvakultúry</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Soil'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/so">Pôda</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Pôda'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/so">Pôda</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Environmental monitoring facilities'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/ef">Zariadenia na monitorovanie životného prostredia</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Zariadenia na monitorovanie životného prostredia'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/ef">Zariadenia na monitorovanie životného prostredia</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Administrative units'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/au">Správne jednotky</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Správne jednotky'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/au">Správne jednotky</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Administratívne jednotky'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/au">Správne jednotky</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Addresses'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/ad">Adresy</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Adresy'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/ad">Adresy</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Statistical units'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/su">Štatistické jednotky</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Štatistické jednotky'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/su">Štatistické jednotky</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Geographical names'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/gn">Zemepisné názvy</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Zemepisné názvy'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/gn">Zemepisné názvy</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Geografické názvy'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/gn">Zemepisné názvy</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Land use'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/lu">Využitie územia</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Využitie územia'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/lu">Využitie územia</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Orthoimagery'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/oi">Ortometria</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Ortometria'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/oi">Ortometria</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Natural risk zones'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/nz">Zóny prírodného rizika</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Zóny prírodného rizika'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/nz">Zóny prírodného rizika</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Cadastral parcels'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/cp">Katastrálne parcely</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Katastrálne parcely'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/cp">Katastrálne parcely</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Area management/restriction/regulation zones and reporting units'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/am">Spravované/obmedzené/regulované zóny a jednotky podávajúce správy</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Spravované/obmedzené/regulované zóny a jednotky podávajúce správy'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/theme/am">Spravované/obmedzené/regulované zóny a jednotky podávajúce správy</gmx:Anchor>
                    </xsl:if>
                </gmd:keyword>
            </xsl:for-each>
            <gmd:thesaurusName>
                <gmd:CI_Citation>
                    <gmd:title>
                        <gmx:Anchor xlink:href="https://www.eionet.europa.eu/gemet/it/inspire-themes/">GEMET - INSPIRE themes, version 1.0</gmx:Anchor>
                    </gmd:title>
                    <gmd:date>
                        <gmd:CI_Date>
                            <gmd:date>
                                <gco:Date>2008-06-01</gco:Date>
                            </gmd:date>
                            <gmd:dateType>
                                <gmd:CI_DateTypeCode codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_DateTypeCode" codeListValue="publication"/>
                            </gmd:dateType>
                        </gmd:CI_Date>
                    </gmd:date>
                </gmd:CI_Citation>
            </gmd:thesaurusName>
        </gmd:MD_Keywords>
    </xsl:template>

    <!-- Priestorový rozsah kľúčové slová -->
    <xsl:template match="//gmd:descriptiveKeywords/gmd:MD_Keywords[gmd:thesaurusName/gmd:CI_Citation/gmd:title/gco:CharacterString[text() = 'Priestorový rozsah']]">
        <gmd:MD_Keywords>
            <xsl:for-each select="gmd:keyword">
                <gmd:keyword>
                    <xsl:if test="gco:CharacterString = 'Národný'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/national">Národný</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'National'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/national">Národný</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/national'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/national">Národný</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'European'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/european">Európsky</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Európsky'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/european">Európsky</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/european'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/european">Európsky</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Global'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/global">Globálny</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Globálny'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/global">Globálny</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/global'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/global">Globálny</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Local'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/local">Miestny</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Miestny'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/local">Miestny</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/local'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/local">Miestny</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Regional'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/regional">Regionálny</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Regionálny'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/regional">Regionálny</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/regional'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/regional">Regionálny</gmx:Anchor>
                    </xsl:if>
                </gmd:keyword>
            </xsl:for-each>
            <gmd:thesaurusName>
                <gmd:CI_Citation>
                    <gmd:title>
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope">Priestorový rozsah</gmx:Anchor>
                    </gmd:title>
                    <gmd:date>
                        <gmd:CI_Date>
                            <gmd:date>
                                <gco:Date>2019-05-22</gco:Date>
                            </gmd:date>
                            <gmd:dateType>
                                <gmd:CI_DateTypeCode codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_DateTypeCode" codeListValue="publication">publication</gmd:CI_DateTypeCode>
                            </gmd:dateType>
                        </gmd:CI_Date>
                    </gmd:date>
                </gmd:CI_Citation>
            </gmd:thesaurusName>
        </gmd:MD_Keywords>
    </xsl:template>

    <!-- Priestorový rozsah kľúčové slová UGKK -->
    <xsl:template match="//gmd:descriptiveKeywords/gmd:MD_Keywords[gmd:keyword/gco:CharacterString[text() = 'http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/national']]">
        <gmd:MD_Keywords>
            <xsl:for-each select="gmd:keyword">
                <gmd:keyword>
                    <xsl:if test="gco:CharacterString = 'Národný'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/national">Národný</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'National'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/national">Národný</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/national'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/national">Národný</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'European'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/european">Európsky</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Európsky'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/european">Európsky</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/european'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/european">Európsky</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Global'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/global">Globálny</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Globálny'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/global">Globálny</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/global'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/global">Globálny</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Local'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/local">Miestny</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Miestny'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/local">Miestny</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/local'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/local">Miestny</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Regional'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/regional">Regionálny</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'Regionálny'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/regional">Regionálny</gmx:Anchor>
                    </xsl:if>
                    <xsl:if test="gco:CharacterString = 'http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/regional'">
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope/regional">Regionálny</gmx:Anchor>
                    </xsl:if>
                </gmd:keyword>
            </xsl:for-each>
            <gmd:thesaurusName>
                <gmd:CI_Citation>
                    <gmd:title>
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialScope">Priestorový rozsah</gmx:Anchor>
                    </gmd:title>
                    <gmd:date>
                        <gmd:CI_Date>
                            <gmd:date>
                                <gco:Date>2019-05-22</gco:Date>
                            </gmd:date>
                            <gmd:dateType>
                                <gmd:CI_DateTypeCode codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_DateTypeCode" codeListValue="publication">publication</gmd:CI_DateTypeCode>
                            </gmd:dateType>
                        </gmd:CI_Date>
                    </gmd:date>
                </gmd:CI_Citation>
            </gmd:thesaurusName>
        </gmd:MD_Keywords>
    </xsl:template>

    <!-- Prazdny title name slovnika = odstranenie citacie slovnika -->
    <xsl:template match="//gmd:descriptiveKeywords/gmd:MD_Keywords[gmd:thesaurusName/gmd:CI_Citation/gmd:title/gco:CharacterString[string-length(text()) = 0]]">
        <xsl:comment>
            <xsl:value-of select="'prazdny slovnik'"/>
        </xsl:comment>
        <gmd:MD_Keywords>
            <xsl:for-each select="gmd:keyword">
                <gmd:keyword>
                    <gco:CharacterString>
                        <xsl:value-of select="gco:CharacterString"/>
                    </gco:CharacterString>
                </gmd:keyword>
            </xsl:for-each>
        </gmd:MD_Keywords>
    </xsl:template>

    <!-- Kľúčové slová service  -->
    <xsl:template match="//srv:SV_ServiceIdentification/gmd:descriptiveKeywords/gmd:MD_Keywords/gmd:keyword/gco:CharacterString">
        <xsl:choose>
            <xsl:when test="text() = 'humanInteractionService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/humanInteractionService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'humanCatalogueViewer'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/humanCatalogueViewer">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'humanGeographicViewer'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/humanGeographicViewer">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'humanGeographicSpreadsheetViewer'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/humanGeographicSpreadsheetViewer">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'humanServiceEditor'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/humanServiceEditor">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'humanChainDefinitionEditor'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/humanChainDefinitionEditor">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'humanWorkflowEnactmentManager'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/humanWorkflowEnactmentManager">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'humanGeographicFeatureEditor'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/humanGeographicFeatureEditor">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'humanGeographicSymbolEditor'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/humanGeographicSymbolEditor">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'humanFeatureGeneralizationEditor'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/humanFeatureGeneralizationEditor">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'humanGeographicDataStructureViewer'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/humanGeographicDataStructureViewer">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'infoManagementService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/infoManagementService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'infoFeatureAccessService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/infoFeatureAccessService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'infoMapAccessService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/infoMapAccessService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'infoCoverageAccessService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/infoCoverageAccessService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'infoSensorDescriptionService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/infoSensorDescriptionService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'infoProductAccessService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/infoProductAccessService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'infoFeatureTypeService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/infoFeatureTypeService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'infoCatalogueService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/infoCatalogueService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'infoGazetteerService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/infoGazetteerService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'infoOrderHandlingService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/infoOrderHandlingService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'infoStandingOrderService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/infoStandingOrderService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'taskManagementService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/taskManagementService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'chainDefinitionService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/chainDefinitionService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'workflowEnactmentService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/workflowEnactmentService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'subscriptionService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/subscriptionService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'spatialProcessingService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/spatialProcessingService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'spatialCoordinateConversionService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/spatialCoordinateConversionService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'spatialCoordinateTransformationService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/spatialCoordinateTransformationService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'spatialCoverageVectorConversionService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/spatialCoverageVectorConversionService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'spatialImageCoordinateConversionService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/spatialImageCoordinateConversionService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'spatialRectificationService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/spatialRectificationService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'spatialOrthorectificationService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/spatialOrthorectificationService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'spatialSensorGeometryModelAdjustmentService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/spatialSensorGeometryModelAdjustmentService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'spatialImageGeometryModelConversionService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/spatialImageGeometryModelConversionService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'spatialSubsettingService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/spatialSubsettingService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'spatialSamplingService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/spatialSamplingService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'spatialTilingChangeService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/spatialTilingChangeService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'spatialDimensionMeasurementService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/spatialDimensionMeasurementService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'spatialFeatureManipulationService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/spatialFeatureManipulationService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'spatialFeatureMatchingService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/spatialFeatureMatchingService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'spatialFeatureGeneralizationService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/spatialFeatureGeneralizationService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'spatialRouteDeterminationService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/spatialRouteDeterminationService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'spatialPositioningService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/spatialPositioningService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'spatialProximityAnalysisService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/spatialProximityAnalysisService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'thematicProcessingService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/thematicProcessingService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'thematicGoparameterCalculationService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/thematicGoparameterCalculationService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'thematicClassificationService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/thematicClassificationService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'thematicFeatureGeneralizationService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/thematicFeatureGeneralizationService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'thematicSubsettingService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/thematicSubsettingService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'thematicSpatialCountingService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/thematicSpatialCountingService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'thematicChangeDetectionService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/thematicChangeDetectionService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'thematicGeographicInformationExtractionService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/thematicGeographicInformationExtractionService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'thematicImageProcessingService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/thematicImageProcessingService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'thematicReducedResolutionGenerationService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/thematicReducedResolutionGenerationService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'thematicImageManipulationService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/thematicImageManipulationService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'thematicImageUnderstandingService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/thematicImageUnderstandingService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'thematicImageSynthesisService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/thematicImageSynthesisService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'thematicMultibandImageManipulationService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/thematicMultibandImageManipulationService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'thematicObjectDetectionService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/thematicObjectDetectionService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'thematicGeoparsingService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/thematicGeoparsingService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'thematicGeocodingService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/thematicGeocodingService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'temporalProcessingService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/temporalProcessingService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'temporalReferenceSystemTransformationService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/temporalReferenceSystemTransformationService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'temporalSubsettingService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/temporalSubsettingService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'temporalSamplingService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/temporalSamplingService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'temporalProximityAnalysisService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/temporalProximityAnalysisService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'metadataProcessingService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/metadataProcessingService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'metadataStatisticalCalculationService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/metadataStatisticalCalculationService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'metadataGeographicAnnotationService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/metadataGeographicAnnotationService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'comService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/comService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'comEncodingService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/comEncodingService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'comTransferService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/comTransferService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'comGeographicCompressionService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/comGeographicCompressionService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'comGeographicFormatConversionService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/comGeographicFormatConversionService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'comMessagingService'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/comMessagingService">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:when test="text() = 'comRemoteFileAndExecutableManagement'">
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/comRemoteFileAndExecutableManagement">
                    <xsl:value-of select="text()"/>
                </gmx:Anchor>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:value-of select="."/>
                </xsl:copy>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!--  Typ služby -->
    <xsl:template match="//gmd:identificationInfo/srv:SV_ServiceIdentification/srv:serviceType/gco:LocalName">
        <gco:LocalName codeSpace="http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceType/{$serviceType}">
            <xsl:value-of select="$serviceType"/>
        </gco:LocalName>
    </xsl:template>

    <!-- Doplnenie popisu urovne kvality pri službe -->
    <xsl:template match="//gmd:MD_Metadata/gmd:dataQualityInfo/gmd:DQ_DataQuality/gmd:scope/gmd:DQ_Scope/gmd:levelDescription">
        <!--  <xsl:copy>
            <xsl:apply-templates select="@* | node()"/>
        </xsl:copy>  -->
        <xsl:choose>
            <xsl:when test="$hierarchyLevel = 'service'">
                <gmd:levelDescription>
                    <gmd:MD_ScopeDescription>
                        <gmd:other>
                            <gco:CharacterString>service</gco:CharacterString>
                        </gmd:other>
                    </gmd:MD_ScopeDescription>
                </gmd:levelDescription>
            </xsl:when>
            <xsl:when test="$hierarchyLevel = 'dataset'">
                <gmd:levelDescription>
                    <gmd:MD_ScopeDescription>
                        <gmd:other>
                            <gco:CharacterString>dataset</gco:CharacterString>
                        </gmd:other>
                    </gmd:MD_ScopeDescription>
                </gmd:levelDescription>
            </xsl:when>
            <xsl:when test="$hierarchyLevel = 'series'">
                <gmd:levelDescription>
                    <gmd:MD_ScopeDescription>
                        <gmd:other>
                            <gco:CharacterString>series</gco:CharacterString>
                        </gmd:other>
                    </gmd:MD_ScopeDescription>
                </gmd:levelDescription>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy-of select="descendant-or-self::gmd:levelDescription"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <!-- uprava ConditionsApplyingToAccessAndUse -->

    <xsl:template match="//gmd:resourceConstraints[gmd:MD_Constraints/gmd:useLimitation]">
        <gmd:resourceConstraints>
            <gmd:MD_LegalConstraints>
                <gmd:useConstraints>
                    <gmd:MD_RestrictionCode codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_RestrictionCode" codeListValue="otherRestrictions"/>
                </gmd:useConstraints>
                <xsl:choose>
                    <xsl:when test="translate(gmd:MD_Constraints/gmd:useLimitation/gco:CharacterString, $uppercase, $lowercase) = 'no conditions apply'">
                        <gmd:otherConstraints>
                            <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/ConditionsApplyingToAccessAndUse/noConditionsApply">neuplatňujú sa žiadne podmienky</gmx:Anchor>
                        </gmd:otherConstraints>
                    </xsl:when>
                    <xsl:when test="translate(gmd:MD_Constraints/gmd:useLimitation/gco:CharacterString, $uppercase, $lowercase) = 'neuplatňujú sa žiadne podmienky'">
                        <gmd:otherConstraints>
                            <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/ConditionsApplyingToAccessAndUse/noConditionsApply">neuplatňujú sa žiadne podmienky</gmx:Anchor>
                        </gmd:otherConstraints>
                    </xsl:when>
                    <xsl:when test="translate(gmd:MD_Constraints/gmd:useLimitation/gco:CharacterString, $uppercase, $lowercase) = 'conditions unknown'">
                        <gmd:otherConstraints>
                            <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/ConditionsApplyingToAccessAndUse/conditionsUnknown">podmienky neznáme</gmx:Anchor>
                        </gmd:otherConstraints>
                    </xsl:when>
                    <xsl:when test="translate(gmd:MD_Constraints/gmd:useLimitation/gco:CharacterString, $uppercase, $lowercase) = 'conditions unknown'">
                        <gmd:otherConstraints>
                            <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/ConditionsApplyingToAccessAndUse/conditionsUnknown">podmienky neznáme</gmx:Anchor>
                        </gmd:otherConstraints>
                    </xsl:when>
                    <xsl:when test="translate(gmd:MD_Constraints/gmd:useLimitation/gco:CharacterString, $uppercase, $lowercase) = 'neznáme podmienky'">
                        <gmd:otherConstraints>
                            <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/ConditionsApplyingToAccessAndUse/conditionsUnknown">podmienky neznáme</gmx:Anchor>
                        </gmd:otherConstraints>
                    </xsl:when>
                    <xsl:when test="translate(gmd:MD_Constraints/gmd:useLimitation/gco:CharacterString, $uppercase, $lowercase) = 'podmienky neznáme'">
                        <gmd:otherConstraints>
                            <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/ConditionsApplyingToAccessAndUse/conditionsUnknown">podmienky neznáme</gmx:Anchor>
                        </gmd:otherConstraints>
                    </xsl:when>
                    <xsl:when test="translate(gmd:MD_Constraints/gmd:useLimitation/gco:CharacterString, $uppercase, $lowercase) = 'inspire základná licencia'">
                        <gmd:otherConstraints>
                            <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/ConditionsApplyingToAccessAndUse/noConditionsApply">neuplatňujú sa žiadne podmienky</gmx:Anchor>
                        </gmd:otherConstraints>
                    </xsl:when>
                    <!-- ak je uvedena prazdna polozka, tak sa nastavi podmienky nezname -->
                    <xsl:when test="gmd:MD_Constraints/gmd:useLimitation/gco:CharacterString[string-length(normalize-space(text())) = 0]">
                        <gmd:otherConstraints>
                            <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/ConditionsApplyingToAccessAndUse/conditionsUnknown">podmienky neznáme</gmx:Anchor>
                        </gmd:otherConstraints>
                    </xsl:when>
                    <xsl:otherwise>
                        <gmd:otherConstraints>
                            <gco:CharacterString>
                                <xsl:value-of select="gmd:MD_Constraints/gmd:useLimitation/gco:CharacterString"/>
                            </gco:CharacterString>
                        </gmd:otherConstraints>
                    </xsl:otherwise>
                </xsl:choose>
            </gmd:MD_LegalConstraints>
        </gmd:resourceConstraints>
    </xsl:template>

    <!-- uprava LimitationsOnPublicAccess -->

    <xsl:template match="gmd:resourceConstraints/gmd:MD_LegalConstraints[gmd:otherConstraints[preceding::gmd:accessConstraints/gmd:MD_RestrictionCode/@codeListValue = 'otherRestrictions']]">
        <gmd:MD_LegalConstraints>
            <gmd:accessConstraints>
                <gmd:MD_RestrictionCode codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#MD_RestrictionCode" codeListValue="otherRestrictions">otherRestrictions</gmd:MD_RestrictionCode>
            </gmd:accessConstraints>

            <xsl:choose>
                <xsl:when test="translate(gmd:otherConstraints/gco:CharacterString, $uppercase, $lowercase) = 'bez obmedzenia prístupu'">
                    <gmd:otherConstraints>
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/LimitationsOnPublicAccess/noLimitations">Bez obmedzenia prístupu</gmx:Anchor>
                    </gmd:otherConstraints>
                </xsl:when>
                <xsl:when test="translate(gmd:otherConstraints/gco:CharacterString, $uppercase, $lowercase) = 'bez obmedzenia prístupu.'">
                    <gmd:otherConstraints>
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/LimitationsOnPublicAccess/noLimitations">Bez obmedzenia prístupu</gmx:Anchor>
                    </gmd:otherConstraints>
                </xsl:when>
                <xsl:when test="translate(gmd:otherConstraints/gco:CharacterString, $uppercase, $lowercase) = 'bez obmedzenia'">
                    <gmd:otherConstraints>
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/LimitationsOnPublicAccess/noLimitations">Bez obmedzenia prístupu</gmx:Anchor>
                    </gmd:otherConstraints>
                </xsl:when>
                <xsl:when test="translate(gmd:otherConstraints/gco:CharacterString, $uppercase, $lowercase) = 'no limitations'">
                    <gmd:otherConstraints>
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/LimitationsOnPublicAccess/noLimitations">Bez obmedzenia prístupu</gmx:Anchor>
                    </gmd:otherConstraints>
                </xsl:when>
                <xsl:when test="translate(gmd:otherConstraints/gco:CharacterString, $uppercase, $lowercase) = 'nie sú.'">
                    <gmd:otherConstraints>
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/LimitationsOnPublicAccess/noLimitations">Bez obmedzenia prístupu</gmx:Anchor>
                    </gmd:otherConstraints>
                </xsl:when>
                <xsl:when test="translate(gmd:otherConstraints/gco:CharacterString, $uppercase, $lowercase) = 'žiadne podmienky sa neaplikujú'">
                    <gmd:otherConstraints>
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/LimitationsOnPublicAccess/noLimitations">Bez obmedzenia prístupu</gmx:Anchor>
                    </gmd:otherConstraints>
                </xsl:when>
                <xsl:when test="translate(gmd:otherConstraints/gco:CharacterString, $uppercase, $lowercase) = 'žiadne obmedzenia'">
                    <gmd:otherConstraints>
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/LimitationsOnPublicAccess/noLimitations">Bez obmedzenia prístupu</gmx:Anchor>
                    </gmd:otherConstraints>
                </xsl:when>
                <xsl:when test="translate(gmd:otherConstraints/gco:CharacterString, $uppercase, $lowercase) = 'http://inspire.ec.europa.eu/metadata-codelist/limitationsonpublicaccess/nolimitations'">
                    <gmd:otherConstraints>
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/LimitationsOnPublicAccess/noLimitations">Bez obmedzenia prístupu</gmx:Anchor>
                    </gmd:otherConstraints>
                </xsl:when>
                 <xsl:when test="translate(gmd:otherConstraints/gco:CharacterString, $uppercase, $lowercase) = 'http://inspire.ec.europa.eu/metadata-codelist/limitationsonpublicaccess/inspire_directive_article13_1e'">
                    <gmd:otherConstraints>
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/LimitationsOnPublicAccess/INSPIRE_Directive_Article13_1e">Práva duševného vlastníctva</gmx:Anchor>
                    </gmd:otherConstraints>
                </xsl:when>
                <xsl:when test="translate(gmd:otherConstraints/gco:CharacterString, $uppercase, $lowercase) = 'http://inspire.ec.europa.eu/metadata-codelist/limitationsonpublicaccess/inspire_directive_article13_1a'">
                    <gmd:otherConstraints>
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/LimitationsOnPublicAccess/INSPIRE_Directive_Article13_1a">Dôvernosť konania orgánov verejnej moci, ak je takáto dôvernosť ustanovená právnym predpisom</gmx:Anchor>
                    </gmd:otherConstraints>
                </xsl:when>
                <xsl:when test="translate(gmd:otherConstraints/gco:CharacterString, $uppercase, $lowercase) = 'neznáme podmienky'">
                    <gmd:otherConstraints>
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/LimitationsOnPublicAccess/noLimitations">Bez obmedzenia prístupu</gmx:Anchor>
                    </gmd:otherConstraints>
                </xsl:when>
                <xsl:when test="translate(gmd:otherConstraints/gco:CharacterString, $uppercase, $lowercase) = 'bez obmedzenia s rešpektovaním autorských práv'">
                    <gmd:otherConstraints>
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/LimitationsOnPublicAccess/noLimitations">Bez obmedzenia prístupu</gmx:Anchor>
                    </gmd:otherConstraints>
                </xsl:when>
                
                
                <!-- SSC:doplnenie pre validitu -->
           <!--       <xsl:when test="translate(gmd:otherConstraints/gco:CharacterString, $uppercase, $lowercase) = 'license: creative commons attribution'">
                    <gmd:otherConstraints>
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/LimitationsOnPublicAccess/noLimitations">Bez obmedzenia prístupu</gmx:Anchor>
                    </gmd:otherConstraints>
                </xsl:when> -->
                
                <!-- uprava pre MZP SR  -->
                <xsl:when test="translate(gmd:otherConstraints/gco:CharacterString, $uppercase, $lowercase) = 'neexistujú obmedzenia prístupu verejnosti cc-by https://creativecommons.org/licenses/by/4.0/'">
                    <gmd:otherConstraints>
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/LimitationsOnPublicAccess/noLimitations">Bez obmedzenia prístupu</gmx:Anchor>
                    </gmd:otherConstraints>
                </xsl:when>
                 <xsl:when test="translate(gmd:otherConstraints/gco:CharacterString, $uppercase, $lowercase) = 'neexistujú obmedzenia prístupu verejnosti'">
                    <gmd:otherConstraints>
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/LimitationsOnPublicAccess/noLimitations">Bez obmedzenia prístupu</gmx:Anchor>
                    </gmd:otherConstraints>
                </xsl:when>
                
                
                <!-- ak je uvedena prazdna polozka, tak sa nastavi Bez obmedzeni pristupu -->
                <xsl:when test="gmd:otherConstraints/gco:CharacterString[string-length(normalize-space(text())) = 0]">
                    <gmd:otherConstraints>
                        <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/metadata-codelist/LimitationsOnPublicAccess/noLimitations">Bez obmedzenia prístupu</gmx:Anchor>
                    </gmd:otherConstraints>
                </xsl:when>
                <xsl:otherwise>


                    <xsl:copy-of select="descendant-or-self::gmd:otherConstraints"/>

                    <!--  <gmd:otherConstraints>
                        <gco:CharacterString>
                            <xsl:value-of select="gmd:otherConstraints/gco:CharacterString"/>
                        </gco:CharacterString>
                    </gmd:otherConstraints>  -->
                </xsl:otherwise>
            </xsl:choose>
        </gmd:MD_LegalConstraints>
    </xsl:template>

    <!-- Suradnicovy system -->

    <xsl:template match="//gmd:referenceSystemIdentifier/gmd:RS_Identifier">
        <xsl:variable name="code" select="gmd:code/gco:CharacterString"/>
        <xsl:variable name="title" select="gmd:authority/gmd:CI_Citation/gmd:title/gco:CharacterString"/>
        <gmd:RS_Identifier>
            <xsl:choose>
                <xsl:when test="$code != ''">
                    <gmd:code>
                        <xsl:if test="$title != ''">
                            <gmx:Anchor xlink:href="{$code}">
                                <xsl:value-of select="$title"/>
                            </gmx:Anchor>
                        </xsl:if>
                        <xsl:if test="string-length($title) = 0">
                            <gmx:Anchor xlink:href="{$code}">EPSG:<xsl:value-of select="substring($code, string-length($code) - 3)"/></gmx:Anchor>
                        </xsl:if>
                    </gmd:code>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:apply-templates select="@* | node()"/>
                </xsl:otherwise>

            </xsl:choose>
        </gmd:RS_Identifier>
    </xsl:template>

    <!-- Vymena RS Identifier za MD Identifier -->
    <xsl:template match="gmd:MD_Metadata/gmd:identificationInfo/*/gmd:citation/*/gmd:identifier/gmd:RS_Identifier">
        <gmd:MD_Identifier>
            <xsl:apply-templates select="gmd:code"/>
        </gmd:MD_Identifier>
    </xsl:template>

    <!-- špecifikácia -->
    <xsl:template match="//gmd:dataQualityInfo/gmd:DQ_DataQuality/gmd:report/gmd:DQ_DomainConsistency/gmd:result/gmd:DQ_ConformanceResult/gmd:specification">
        <xsl:comment>
            <xsl:value-of select="'Úprava špecifikácia'"/>
        </xsl:comment>
        <xsl:choose>
            <xsl:when test="gmd:CI_Citation/gmd:title[contains(gco:CharacterString, '976/2009')]">
                <gmd:specification>
                    <gmd:CI_Citation>
                        <gmd:title>
                            <gmx:Anchor xlink:href="http://eur-lex.europa.eu/eli/reg/2009/976">NARIADENIE KOMISIE (ES) č. 976/2009 z 19. októbra 2009, ktorým sa vykonáva smernica Európskeho parlamentu a Rady 2007/2/ES, pokiaľ ide o sieťové služby</gmx:Anchor>
                        </gmd:title>
                        <gmd:alternateTitle>
                            <gco:CharacterString>COMMISSION REGULATION (EC) No 976/2009 of 19 October 2009 implementing Directive 2007/2/EC of the European Parliament and of the Council as regards the Network Services</gco:CharacterString>
                        </gmd:alternateTitle>
                        <gmd:date>
                            <gmd:CI_Date>
                                <gmd:date>
                                    <gco:Date>2009-10-20</gco:Date>
                                </gmd:date>
                                <gmd:dateType>
                                    <gmd:CI_DateTypeCode codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_DateTypeCode" codeListValue="publication">publication</gmd:CI_DateTypeCode>
                                </gmd:dateType>
                            </gmd:CI_Date>
                        </gmd:date>
                    </gmd:CI_Citation>
                </gmd:specification>
            </xsl:when>
            <xsl:when test="gmd:CI_Citation/gmd:title[contains(gco:CharacterString, '1089/2010')]">
                <gmd:specification>
                    <gmd:CI_Citation>
                        <gmd:title>
                            <gmx:Anchor xlink:href="http://eur-lex.europa.eu/eli/reg/2010/1089">Nariadenie Komisie (EÚ) č. 1089/2010 z 23. novembra 2010, ktorým sa vykonáva smernica Európskeho parlamentu a Rady 2007/2/ES, pokiaľ ide o interoperabilitu súborov a služieb priestorových údajov</gmx:Anchor>
                        </gmd:title>
                        <gmd:alternateTitle>
                            <gco:CharacterString>Commission Regulation (EU) No 1089/2010 of 23 November 2010 implementing Directive 2007/2/EC of the European Parliament and of the Council as regards interoperability of spatial data sets and services</gco:CharacterString>
                        </gmd:alternateTitle>
                        <gmd:date>
                            <gmd:CI_Date>
                                <gmd:date>
                                    <gco:Date>2010-12-08</gco:Date>
                                </gmd:date>
                                <gmd:dateType>
                                    <gmd:CI_DateTypeCode codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_DateTypeCode" codeListValue="publication">publication</gmd:CI_DateTypeCode>
                                </gmd:dateType>
                            </gmd:CI_Date>
                        </gmd:date>
                    </gmd:CI_Citation>
                </gmd:specification>
            </xsl:when>
            <!-- uprava pre VUVH -->
            <xsl:when test="gmd:CI_Citation/gmd:title/gco:CharacterString[string-length(text()) = 0]">
                <gmd:specification>
                    <gmd:CI_Citation>
                        <gmd:title>
                            <gmx:Anchor xlink:href="http://eur-lex.europa.eu/eli/reg/2010/1089">Nariadenie Komisie (EÚ) č. 1089/2010 z 23. novembra 2010, ktorým sa vykonáva smernica Európskeho parlamentu a Rady 2007/2/ES, pokiaľ ide o interoperabilitu súborov a služieb priestorových údajov</gmx:Anchor>
                        </gmd:title>
                        <gmd:alternateTitle>
                            <gco:CharacterString>Commission Regulation (EU) No 1089/2010 of 23 November 2010 implementing Directive 2007/2/EC of the European Parliament and of the Council as regards interoperability of spatial data sets and services</gco:CharacterString>
                        </gmd:alternateTitle>
                        <gmd:date>
                            <gmd:CI_Date>
                                <gmd:date>
                                    <gco:Date>2010-12-08</gco:Date>
                                </gmd:date>
                                <gmd:dateType>
                                    <gmd:CI_DateTypeCode codeList="http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#CI_DateTypeCode" codeListValue="publication">publication</gmd:CI_DateTypeCode>
                                </gmd:dateType>
                            </gmd:CI_Date>
                        </gmd:date>
                    </gmd:CI_Citation>
                </gmd:specification>
            </xsl:when>
            <xsl:otherwise>
                <gmd:specification>
                    <xsl:apply-templates select="@* | node()"/>
                </gmd:specification>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

    <xsl:template match="/gmd:MD_Metadata/gmd:dataQualityInfo/gmd:DQ_DataQuality/gmd:report/gmd:DQ_DomainConsistency/gmd:result/gmd:DQ_ConformanceResult/gmd:pass">
        <xsl:choose>
            <xsl:when test="not(node())">
                <gmd:pass gco:nilReason="unknown"/>
            </xsl:when>
            <xsl:otherwise>
                <gmd:pass>
                    <xsl:apply-templates select="@* | node()"/>
                </gmd:pass>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>


    <!-- uprava hodnoty codelist, lebo stara referencia uz neplati -->
    <xsl:template match="@codeList[starts-with(., 'https://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources')]">
        <xsl:attribute name="codeList">
            <xsl:text>http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#</xsl:text>
            <xsl:value-of select="substring-after(., '#')"/>
        </xsl:attribute>
    </xsl:template>

    <xsl:template match="@codeList[starts-with(., 'http://standards.iso.org/ittf/PubliclyAvailableStandards/ISO_19139_Schemas/resources')]">
        <xsl:attribute name="codeList">
            <xsl:text>http://standards.iso.org/iso/19139/resources/gmxCodelists.xml#</xsl:text>
            <xsl:value-of select="substring-after(., '#')"/>
        </xsl:attribute>
    </xsl:template>
    
    <!-- uprava hodnoty codelist, lebo stara referencia uz neplati 
    <xsl:template match="gmd:language/gmd:LanguageCode/@codeList">
        <xsl:attribute name="codeList">
            <xsl:text>http://www.loc.gov/standards/iso639-2/</xsl:text>
           
        </xsl:attribute>
    </xsl:template> -->

    <!-- distribučný formát -->
    <xsl:template match="//gmd:distributionInfo/gmd:MD_Distribution/gmd:distributionFormat/gmd:MD_Format/gmd:name/gco:CharacterString">
        <xsl:choose>
            <xsl:when test="contains(translate(normalize-space(text()), $uppercase, $lowercase), 'shp')">
                <!--    <xsl:when test="contains(lower-case(normalize-space(text())),'shp')"> -->
                <gmx:Anchor xlink:href="http://www.iana.org/assignments/media-types/application/vnd.shp">vnd.shp</gmx:Anchor>
            </xsl:when>
            <xsl:when test="contains(translate(normalize-space(text()), $uppercase, $lowercase), 'shape')">
                <!--    <xsl:when test="contains(lower-case(normalize-space(text())),'shape')"> -->
                <gmx:Anchor xlink:href="http://www.iana.org/assignments/media-types/application/vnd.shp">vnd.shp</gmx:Anchor>
            </xsl:when>
            <xsl:when test="contains(translate(normalize-space(text()), $uppercase, $lowercase), 'gml')">
                <!--      <xsl:when test="contains(lower-case(normalize-space(text())),'gml')"> -->
                <gmx:Anchor xlink:href="http://www.iana.org/assignments/media-types/application/gml+xml">gml+xml</gmx:Anchor>
            </xsl:when>
            <xsl:when test="contains(translate(normalize-space(text()), $uppercase, $lowercase), 'las')">
                <!--       <xsl:when test="contains(lower-case(normalize-space(text())),'las')"> -->
                <gmx:Anchor xlink:href="http://www.iana.org/assignments/media-types/application/vnd.las">vnd.las</gmx:Anchor>
            </xsl:when>
            <xsl:when test="contains(translate(normalize-space(text()), $uppercase, $lowercase), 'esri gdb')">
                <!--         <xsl:when test="contains(lower-case(normalize-space(text())),'esri gdb')"> -->
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/media-types/application/x-filegdb">x-filegdb</gmx:Anchor>
            </xsl:when>
            <xsl:when test="contains(translate(normalize-space(text()), $uppercase, $lowercase), 'esri geodatab')">
                <!--      <xsl:when test="contains(lower-case(normalize-space(text())),'esri geodatab')"> -->
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/media-types/application/x-filegdb">x-filegdb</gmx:Anchor>
            </xsl:when>
            <xsl:when test="contains(translate(normalize-space(text()), $uppercase, $lowercase), 'oracle')">
                <!--         <xsl:when test="contains(lower-case(normalize-space(text())),'oracle')"> -->
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/media-types/application/x-oracledump">x-oracledump</gmx:Anchor>
            </xsl:when>
            <xsl:when test="contains(translate(normalize-space(text()), $uppercase, $lowercase), 'asc')">
                <!--       <xsl:when test="contains(lower-case(normalize-space(text())),'asc')"> -->
                <gmx:Anchor xlink:href="http://inspire.ec.europa.eu/media-types/application/x-ascii-grid">x-ascii-grid</gmx:Anchor>
            </xsl:when>
            <xsl:when test="contains(translate(normalize-space(text()), $uppercase, $lowercase), 'tif')">
                <!--    <xsl:when test="contains(lower-case(normalize-space(text())),'tif')"> -->
                <gmx:Anchor xlink:href="http://www.iana.org/assignments/media-types/image/tiff">tiff</gmx:Anchor>
            </xsl:when>
            <xsl:when test="contains(translate(normalize-space(text()), $uppercase, $lowercase), 'csv')">
                <!--     <xsl:when test="contains(lower-case(normalize-space(text())),'csv')"> -->
                <gmx:Anchor xlink:href="http://www.iana.org/assignments/media-types/text/csv">csv</gmx:Anchor>
            </xsl:when>
            <xsl:when test="string-length(text()) = 0 ">
                <!--     <xsl:when test="contains(lower-case(normalize-space(text())),'csv')"> -->
                <gco:CharacterString>unknown</gco:CharacterString>
            </xsl:when>
            <xsl:otherwise>
                <gco:CharacterString>
                    <xsl:value-of select="text()"/>
                </gco:CharacterString>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <!-- uprava verzie distribucneho formatu pokial je v XML prazdna -->
    <xsl:template match="//gmd:distributionInfo/gmd:MD_Distribution/gmd:distributionFormat/gmd:MD_Format/gmd:version">
        <xsl:choose>
            <xsl:when test="gco:CharacterString[string-length(text()) = 0]">
                <gmd:version gco:nilReason="unknown"/>
            </xsl:when>
            <xsl:otherwise>
                <gmd:version>
                    <gco:CharacterString>
                        <xsl:value-of select="gco:CharacterString/text()"/>
                    </gco:CharacterString>
                </gmd:version>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
    <!-- pôvod údajov - úprava pre MZP -->
    <xsl:template match="//gmd:dataQualityInfo/gmd:DQ_DataQuality/gmd:lineage/gmd:LI_Lineage/gmd:statement/gco:CharacterString">
        <xsl:choose>            
            <xsl:when test="string-length(text()) = 0 ">                
                <gco:CharacterString>bude doplnené</gco:CharacterString>
            </xsl:when>
            <xsl:otherwise>
                <gco:CharacterString>
                    <xsl:value-of select="text()"/>
                </gco:CharacterString>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
     <!-- prázdna URL - úprava pre MZP -->
    <xsl:template match="//gmd:distributionInfo/gmd:MD_Distribution/gmd:transferOptions/gmd:MD_DigitalTransferOptions/gmd:onLine/gmd:CI_OnlineResource/gmd:linkage/gmd:URL">
        <xsl:choose>            
            <xsl:when test="string-length(text()) = 0 ">                
                <gmd:URL>https://rpi.gov.sk/client/map/</gmd:URL>
            </xsl:when>
            <xsl:when test="contains(translate(normalize-space(text()), $uppercase, $lowercase), 'http://to-be/specified.com')">                
                <gmd:URL>https://rpi.gov.sk/client/map/</gmd:URL>
            </xsl:when>
            <xsl:otherwise>
                <gmd:URL>
                    <xsl:value-of select="text()"/>
                </gmd:URL>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    
     
    
    

</xsl:stylesheet>
